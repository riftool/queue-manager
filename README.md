# Riftool Queue Manager

### QueueConsumer Example
```python
from queue_manager import QueueConsumer

def callback(ch, method, properties, body):
    print(body)

with QueueConsumer(amqp_url='http://queue/url', exchange_name='LOGS', headers={'x-match': 'any', 'product_type': 'file'}) as consumer:
    consumer.start_consuming(message_callback=callback)
```

### QueueProducer Example
```python
from queue_manager import QueueProducer
with QueueProducer(amqp_url='http://queue/url', exchange_name='LOGS') as producer:
    producer.publish(priority=10, message='Message', headers={'x-match': 'any', 'product_type': 'file'})
```