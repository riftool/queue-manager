from pika.exceptions import ChannelClosedByBroker


def ensure_connection(func):
    def func_wrapper(*args, **kwargs):
        while True:
            try:
                return func(*args, **kwargs)
            except ChannelClosedByBroker:
                args[0].__enter__()
                return func(*args, **kwargs)
    return func_wrapper
