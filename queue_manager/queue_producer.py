from pika import BasicProperties

from queue_manager.base_queue_manager import BaseQueueManager
from queue_manager.decorators import ensure_connection


class QueueProducer(BaseQueueManager):

    @ensure_connection
    def publish(self, message, priority=5, headers=None):
        headers = headers if headers else dict()
        properties = BasicProperties(priority=priority, headers=headers)
        self.channel.basic_publish(exchange=self.exchange_name, routing_key='', body=message, properties=properties)
